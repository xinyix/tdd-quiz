package com.twuc.webApp;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StorageTest {

    static Storage createStorageWithEnoughCapacity() {
        return new Storage(20, 20 ,20);
    }

    @Test
    void should_get_a_ticket_when_saving_a_bag() {
        Storage storage = createStorageWithEnoughCapacity();
        Bag bag = new Bag(BagSize.SMALL);

        Ticket ticket = storage.save(bag);
        assertNotNull(ticket);
    }

    @Test
    @SuppressWarnings("ConstantConditions")
    void should_get_a_ticket_when_saving_nothing() {
        Storage storage = createStorageWithEnoughCapacity();
        final Bag nothing = null;

        Ticket ticket = storage.save(nothing, SlotSize.SMALL);
        assertNotNull(ticket);
    }

    @Test
    void should_get_the_bag_when_retrieving_using_valid_ticket() {
        Storage storage = createStorageWithEnoughCapacity();
        Bag savedBag = new Bag(BagSize.SMALL);
        Ticket ticket = storage.save(savedBag);

        Bag retrievedBag = storage.retrieve(ticket);
        assertSame(savedBag, retrievedBag);
    }

    @Test
    void should_get_the_right_bag_when_retrieving_using_valid_ticket_given_there_are_more_than_one_bag_in_storage() {
        Storage storage = createStorageWithEnoughCapacity();
        Bag savedBag = new Bag(BagSize.SMALL);
        Bag anotherSavedBag = new Bag(BagSize.SMALL);

        Ticket ticket = storage.save(savedBag);
        storage.save(anotherSavedBag);

        Bag retrievedBag = storage.retrieve(ticket);
        assertSame(savedBag, retrievedBag);
        assertNotSame(anotherSavedBag, retrievedBag);
    }

    @Test
    void should_get_error_message_when_retrieving_bag_using_invalid_ticket_from_an_empty_storage() {
        Storage emptyStorage = createStorageWithEnoughCapacity();
        Ticket invalidTicket = new Ticket();

        InvalidTicketException invalidTicketException = assertThrows(InvalidTicketException.class, () -> emptyStorage.retrieve(invalidTicket));
        assertEquals("Invalid Ticket", invalidTicketException.getMessage());
    }

    @Test
    void should_get_error_message_when_retrieving_bag_using_ticket_that_has_already_been_used() {
        Storage storage = createStorageWithEnoughCapacity();
        Ticket ticket = storage.save(new Bag(BagSize.SMALL));
        storage.retrieve(ticket);

        InvalidTicketException invalidTicketException = assertThrows(InvalidTicketException.class, () -> storage.retrieve(ticket));
        assertEquals("Invalid Ticket", invalidTicketException.getMessage());
    }

    @Test
    void should_get_error_message_when_using_ticket_that_does_not_have_responding_bag() {
        Storage storage = createStorageWithEnoughCapacity();
        storage.save(new Bag(BagSize.SMALL));
        Ticket ticket = new Ticket();

        InvalidTicketException invalidTicketException = assertThrows(InvalidTicketException.class, () -> storage.retrieve(ticket));
        assertEquals("Invalid Ticket", invalidTicketException.getMessage());
    }

    @Test
    @SuppressWarnings("ConstantConditions")
    void should_get_nothing_when_retrieving_using_valid_ticket_got_when_saving_nothing() {
        Storage storage = createStorageWithEnoughCapacity();
        Bag nothing = null;
        Ticket ticket = storage.save(nothing);

        Bag retrieved = storage.retrieve(ticket);
        assertNull(retrieved);
    }

    @Test
    void should_get_a_ticket_when_saving_a_bag_given_an_empty_storage_with_capacity_2() {
        Storage storage = new Storage(2, 0, 0);
        Bag bag = new Bag(BagSize.SMALL);

        Ticket ticket = storage.save(bag);
        assertNotNull(ticket);
    }

    @Test
    void should_save_first_bag_and_not_save_the_second_when_saving_2_bags_to_empty_storage_with_capacity_1() {
        Storage storage = new Storage(1, 0,0);
        Bag firstBag = new Bag(BagSize.SMALL);
        Bag secondBag = new Bag(BagSize.SMALL);

        Ticket ticket = storage.save(firstBag);
        assertNotNull(ticket);

        InsufficientCapacityException insufficientCapacityException = assertThrows(InsufficientCapacityException.class, () -> storage.save(secondBag));
        assertEquals("Insufficient Capacity", insufficientCapacityException.getMessage());
    }

    @Test
    void should_get_message_when_saving_to_a_full_storage_with_capacity_2() {
        Storage storage = new Storage(2, 0, 0);
        storage.save(new Bag(BagSize.SMALL));
        storage.save(new Bag(BagSize.SMALL));

        Bag bag = new Bag(BagSize.SMALL);
        assertThrows(InsufficientCapacityException.class, () -> storage.save(bag));
    }

    @Test
    void should_save_when_retrieve_from_a_full_storage() {
        Storage storage = new Storage(1, 0 ,0);
        Ticket ticket = storage.save(new Bag(BagSize.SMALL));
        storage.retrieve(ticket);

        assertNotNull(storage.save(new Bag(BagSize.SMALL)));
    }

    @Test
    void should_get_ticket_when_save_small_bag_to_large_slot() {
        Storage storage = new Storage(0, 0, 1);
        Bag smallBag = new Bag(BagSize.SMALL);

        Ticket ticket = storage.save(smallBag, SlotSize.BIG);
        assertNotNull(ticket);
    }

    @Test
    void should_get_error_message_when_save_big_bag_to_a_small_slot() {
        Storage storage = new Storage(2, 2, 2);
        Bag bigBag = new Bag(BagSize.BIG);

        SizeExceededException sizeExceededException = assertThrows(SizeExceededException.class, () -> storage.save(bigBag));
        assertEquals("Cannot save your bag: BIG SMALL", sizeExceededException.getMessage());
    }

    @Test
    void should_get_error_message_when_save_big_bag_to_storage_with_no_big_slot() {
        Storage storage = new Storage(1, 1, 0);
        Bag bigBag = new Bag(BagSize.BIG);

        InsufficientCapacityException insufficientCapacityException = assertThrows(InsufficientCapacityException.class, () -> storage.save(bigBag, SlotSize.BIG));
        assertEquals("Insufficient Capacity", insufficientCapacityException.getMessage());
    }

    @Test
    void should_get_ticket_when_save_a_medium_bag_to_medium_slot() {
        Storage storage = new Storage(0, 1, 0);
        Bag mediumBag = new Bag(BagSize.MEDIUM);

        Ticket ticket = storage.save(mediumBag, SlotSize.MEDIUM);
        assertNotNull(ticket);
    }
}