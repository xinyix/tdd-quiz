package com.twuc.webApp;

public class Bag {
    private BagSize size;

    public BagSize getSize() {
        return size;
    }

    public Bag(BagSize size) {
        this.size = size;
    }
}
