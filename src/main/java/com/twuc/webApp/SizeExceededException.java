package com.twuc.webApp;

public class SizeExceededException extends RuntimeException {
    public SizeExceededException(String message) {
        super(message);
    }
}
