package com.twuc.webApp;

public enum SlotSize {
    SMALL("s"),
    MEDIUM("m"),
    BIG("b");

    private final String size;

    SlotSize(String size) {
        this.size = size;
    }

    public String getSize() {
        return size;
    }
}
