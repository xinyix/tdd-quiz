package com.twuc.webApp;

public enum BagSize {
    SMALL("s"),
    MEDIUM("m"),
    BIG("b");

    private final String size;

    BagSize(String size) {
        this.size = size;
    }

    public String getSize() {
        return size;
    }
}
