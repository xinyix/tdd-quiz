package com.twuc.webApp;

public class InsufficientCapacityException extends RuntimeException{
    public InsufficientCapacityException(String message) {
        super(message);
    }
}
