package com.twuc.webApp;

import java.util.HashMap;
import java.util.Map;

public class Storage {
    private final Map<SlotSize, SlotStorage> slotMap = new HashMap<>();

    public Storage(int smallCapacity, int mediumCapacity, int bigCapacity) {
        SlotStorage smallSlots = new SlotStorage(smallCapacity, SlotSize.SMALL);
        SlotStorage mediumSlots = new SlotStorage(mediumCapacity, SlotSize.MEDIUM);
        SlotStorage bigSlots = new SlotStorage(bigCapacity, SlotSize.BIG);
        slotMap.put(SlotSize.SMALL, smallSlots);
        slotMap.put(SlotSize.MEDIUM, mediumSlots);
        slotMap.put(SlotSize.BIG, bigSlots);

    }

    public Ticket save(Bag bag) {
        return this.save(bag, SlotSize.SMALL);
    }

    public Ticket save(Bag bag, SlotSize slotSize) {
        return slotMap.get(slotSize).save(bag);
    }

    public Bag retrieve(Ticket ticket) {
        return slotMap.get(ticket.getSlotSize()).retrieve(ticket);
    }
}
