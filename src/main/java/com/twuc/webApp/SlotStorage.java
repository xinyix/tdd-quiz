package com.twuc.webApp;

import java.util.HashMap;
import java.util.Map;

class SlotStorage {

    private int capacity;
    private SlotSize slotSize;
    private int slots;

    private Map<Ticket, Bag> storage = new HashMap<>();

    SlotStorage(int capacity, SlotSize slotSize) {
        this.capacity = capacity;
        this.slotSize = slotSize;
        this.slots = capacity;
    }

    Ticket save(Bag bag) {
        if (this.slots > 0) {
            if(bag != null) {
                BagSize bagSize = bag.getSize();
                if ((BagSize.BIG.equals(bagSize) && SlotSize.SMALL.equals(slotSize))
                    || (BagSize.BIG.equals(bagSize) && SlotSize.MEDIUM.equals(slotSize))
                    || (BagSize.MEDIUM.equals(bagSize) && SlotSize.SMALL.equals(slotSize))) {
                    throw new SizeExceededException(String.format("Cannot save your bag: %s %s", bagSize, slotSize));
                }
            }
            Ticket ticket = new Ticket(slotSize);
            this.slots--;
            storage.put(ticket, bag);
            return ticket;
        } else {
            throw new InsufficientCapacityException("Insufficient Capacity");
        }
    }

    Bag retrieve(Ticket ticket) {
        if (!storage.containsKey(ticket)) {
            throw new InvalidTicketException("Invalid Ticket");
        }
        this.slots++;
        return storage.remove(ticket);
    }
}
