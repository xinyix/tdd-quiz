package com.twuc.webApp;

public class Ticket {
    private SlotSize slotSize;

    public Ticket() {
        this(SlotSize.SMALL);
    }

    public Ticket(SlotSize slotSize) {
        this.slotSize = slotSize;
    }

    public SlotSize getSlotSize() {
        return slotSize;
    }

}
