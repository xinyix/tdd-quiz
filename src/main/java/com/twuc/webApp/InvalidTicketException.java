package com.twuc.webApp;

public class InvalidTicketException extends RuntimeException{
    InvalidTicketException(String message) {
        super(message);
    }
}
